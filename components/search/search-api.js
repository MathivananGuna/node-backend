var MongoClient = require('mongodb').MongoClient;
const sourceFile = require('../../index'); // get variable from index.js
const mongoDbUrl = sourceFile.databaseUrl; // assign mongodb url from index.js

module.exports = function (app) {
    app.route('/user/search')
        .post(getSearch)
}

getSearch = (req, res) => {
    MongoClient.connect(mongoDbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db("userDb");
        var query = { name: req.body.name };
        dbo.collection("userList").find(query).toArray(function (err, result) {
            if (err) throw err;
            let resultData = { data: result, status: result.length === 0 ? "failure" : "success" };
            res.json(resultData);
            db.close();
        });
    });
}