var MongoClient = require('mongodb').MongoClient;
const sourceFile = require('../../index'); // get variable from index.js
const mongoDbUrl = sourceFile.databaseUrl; // assign mongodb url from index.js

module.exports = function (app) {
    app.route('/user/file')
        .get(getFile)
        .post(postFile)
}

getFile = (req, res) => {
    console.log(req.body);
}

postFile = (req, res) => {
    MongoClient.connect(mongoDbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db("userDb");
        var query = { name: req.body.details[0].name };
        dbo.collection("userList").find(query).toArray(function (err, result) {
            if (err) throw err;
            if(result.length === 0){
                let resultData = { data: [], status: "failure", message:"User not available" };
                res.json(resultData);
                db.close();
            }
            else{
                var myquery = { "name": req.body.details[0].name };
                var newvalues = { $set: { name: req.body.details[0].name, createdAt: new Date(req.body.details[0].createdAt), updatedAt: new Date(), fileData: req.body.imageBase } };
                dbo.collection("userList").updateOne(myquery, newvalues, function (err, result) {
                    if (err) throw err;
                    console.log("1 document updated");
                    let resultData = { data: "file added successfully", status: "success", message:"file added successfully" }
                    res.json(resultData)    
                    db.close();
                });
            }
        }); 
    });
}