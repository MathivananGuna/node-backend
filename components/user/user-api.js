var MongoClient = require('mongodb').MongoClient;
const sourceFile = require('../../index'); // get variable from index.js
const mongoDbUrl = sourceFile.databaseUrl; // assign mongodb url from index.js

module.exports = function (app) {
    app.route('/user')
        .get(getUser) // view all list
        .post(postUser) // add new list
        .put(putUser) // edit list
        .delete(deleteUser) // delete list
}

getUser = (req, res) => {
    MongoClient.connect(mongoDbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db("userDb");
        dbo.collection("userList").find({}).toArray(function (err, result) {
            if (err) throw err;
            let sendResponse = {
                data: result,
                status: "success"
            }
            res.json(sendResponse);
            db.close();
        });
    });
}

postUser = (req, res) => {
    MongoClient.connect(mongoDbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db("userDb");
        var query = { name: req.body.name };
        dbo.collection("userList").find(query).toArray(function (err, result) {
            if (err) throw err;
            if (result.length === 0) {
                dbo.collection("userList").insertOne(req.body, function (err, res) {
                    if (err) throw err;
                    console.log("1 document inserted"); 
                    db.close();
                });
                res.json({ status: "success", message: "User added successfully" })
            }
            else {
                res.json({ status: "failure", message: "Username not available. Please try with different name" })
                db.close();
            }
        });
    });
}

putUser = (req, res) => {
    MongoClient.connect(mongoDbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db("userDb");
        var query = { name: req.body.name };
        var myquery = { "name": req.body.oldData.name };
        var newvalues = { $set: { name: req.body.name, createdAt: new Date(req.body.oldData.createdAt), updatedAt: new Date() } };
        dbo.collection("userList").find(query).toArray(function (err, result) {
            if (err) throw err;
            if (result.length === 0) {
                console.log("no data")
                dbo.collection("userList").updateOne(myquery, newvalues, function (err, result) {
                    if (err) throw err;
                    console.log("1 document updated");
                    let resultData = { data: "Updated Successfully", status: "success", message:"Updated successfully" }
                    res.json(resultData)    
                    db.close();
                });
            }
            else {
                console.log("data found")
                let resultData = { data: [], status: "failure", message:"Can't update data"};
                res.json(resultData);
                db.close();
            }
        })
        // var myquery = { "name": req.body.oldData.name };
        // var newvalues = { $set: { name: req.body.name, createdAt: new Date(req.body.oldData.createdAt), updatedAt: new Date() } };
        // dbo.collection("userList").updateOne(myquery, newvalues, function (err, result) {
        //     if (err) throw err;
        //     console.log("1 document updated");
        //     let resultData = { data: "Updated Successfully", status: "success" }
        //     res.json(resultData)
        //     db.close();
        // });
    });
}

deleteUser = (req, res) => {
    console.log(req.body);
    MongoClient.connect(mongoDbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        var dbo = db.db("userDb");
        var myquery = { name: req.body[0].name };
        dbo.collection("userList").deleteOne(myquery, function (err, obj) {
            if (err) throw err;
            let resultData = { data: "Deleted Successfully", status: "success" }
            console.log("1 document deleted");
            res.json(resultData)
            db.close();
        });
    });
}