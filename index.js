const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
app.use(cors({ origin: '*' }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE')
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next()
});
// app.use(bodyParser.urlencoded({ extended: false, useUnifiedTopology: true })); // for normal data upload in body parser
app.use(bodyParser.json({ limit: "5mb" })) // for large data parser set limit
app.use(bodyParser.json());

var url = "mongodb://34.122.244.178:30000/admin"; // declare database url
module.exports = { databaseUrl: url }; // export URL to all file

require('./components/user/user-api')(app); // import other files in main file
require('./components/search/search-api')(app);
require('./components/files/file-handle')(app);

let a = 8080
app.listen(a, () => {
    console.log(`Server is listening on port ${a}`);
});


